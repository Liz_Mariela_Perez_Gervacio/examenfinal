import * as React from 'react';

import Card from '@mui/material/Card';
import CardHeader from '@mui/material/CardHeader';
import CardContent from '@mui/material/CardContent';
import Avatar from '@mui/material/Avatar';
import Typography from '@mui/material/Typography';

type Tabla = {
    icono3: any,
   titulo: any,
    para: any,



}

const tal = (props: Tabla) => {
    const { icono3, titulo, para} = props;

    return (
    
        <Card sx={{ backgroundColor: "transparent" , marginTop:"5%" ,border: " #343334 0.5px solid" , borderRadius:"20px" , display:"flex" , width:"90%" }}>
            <CardHeader sx={{ }}
                avatar={
                    <Avatar
                        alt="Remy Sharp"
                        src={icono3}
                        sx={{ width: 70, height: 70 }}
                    />
                }
            />

            <CardContent sx={{ width:"200px"  }}>
                <Typography sx={{ color: "#FFFFFF", width:"200%" }}>
                    {titulo} </Typography>
                <Typography variant="body2" sx={{color: "#B6B6B6", marginTop:"3%", width:"150%" }}>
                    { para}
                </Typography>
            </CardContent> 

        </Card>
    );
}
export default tal