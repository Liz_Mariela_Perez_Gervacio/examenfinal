import * as React from 'react';
import Box from '@mui/material/Box';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import Avatar from '@mui/material/Avatar';
import { Grid } from '@mui/material';
import Image from 'next/image';

type Cuadrado = {
    icono1: any,
    letra: any,
    but: any,
    num1: any,
    num2: any,

}

const cuadro = (props: Cuadrado) => {
    const { icono1, letra, but, num1, num2 } = props;
    return (
        <Card sx={{ backgroundColor:"transparent" ,  maxWidth: 285 ,border: " #343334 0.5px solid" , borderRadius:"20px" }}>
            <CardContent sx={{ display: "flex", alignItems: "center" }} >

                <Avatar  >
                    A
                </Avatar>
                <Typography sx={{ fontWeight: "530s0", color: "#FFFFFF", marginLeft: "6%" , fontSize: "135%"}}>
                    {letra}
                </Typography>
                <Button sx={{ backgroundColor: "#C6C6C6", position: "absolute", color: "#1D1429", borderRadius: "5pxs", textTransform: "none", marginLeft: "9% ", width: "4%", height: "4%" ,fontSize:"75%" }}>{but}</Button>
                <Grid sx={{marginLeft:"9%" }}>
                    <Image alt="" src="/imagenes/icono.svg/" width={10} height={10}></Image>
                </Grid >

            </CardContent>

            <CardContent sx={{ display: "flex" }} >
                <Grid sx={{ width: " 5%", variant: "h5" }}>
                    <Typography sx={{ color: "#FFFFFF", fontWeight: "500", fontSize: "25px" }}>
                        {num1}
                    </Typography>
                    <Typography sx={{ color: "#FFFFFF", fontWeight: "500", fontSize: "20px" }}>
                        {num2}
                    </Typography>
                </Grid>
                <Grid sx={{ marginLeft: "50%" }}>
                    <Image alt = "" src="/imagenes/grafica.svg" height={80} width={130} />
                </Grid>
            </CardContent>
        </Card>
    );
}
export default cuadro