import * as React from 'react';
import { styled } from '@mui/material/styles';
import Card from '@mui/material/Card';
import CardHeader from '@mui/material/CardHeader';
import CardMedia from '@mui/material/CardMedia';
import CardContent from '@mui/material/CardContent';
import CardActions from '@mui/material/CardActions';
import Collapse from '@mui/material/Collapse';
import Avatar from '@mui/material/Avatar';
import IconButton from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';
import { red } from '@mui/material/colors';
import FavoriteIcon from '@mui/icons-material/Favorite';
import ShareIcon from '@mui/icons-material/Share';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import MoreVertIcon from '@mui/icons-material/MoreVert';
import Button from '@mui/material/Button';
import Image from "next/image";

type Cuadrado2 = {
    icono2: string,
    title: string,
    parafo: string,

}

const Cuadrado2 = (props: Cuadrado2) => {
    const { icono2, title, parafo} = props;

    return (
    
        <Card sx={{ maxWidth: 190, backgroundColor: "transparent" , marginTop:"5%" ,border: " #343334 0.5px solid" , borderRadius:"20px"
        }}>
            <CardHeader sx={{ width: "300px" }}
                avatar={
                    <Avatar
                        alt="Remy Sharp"
                        src={icono2}
                        sx={{ width: 70, height: 70 }}
                    />
                }
            />

            <CardContent>
                <Typography sx={{ color: "#FFFFFF", fontSize: "20px", fontWeight: "500" }}>
                    {title}                </Typography>
                <Typography variant="body2" sx={{ color: "#B6B6B6", marginTop:"3%" }}>
                    { parafo}
                </Typography>
            </CardContent>
            <Button sx={{ color: "#0FAE96" , marginLeft:"5%"}} > See Explained  <Image  alt=""src='/imagenes/flecha.svg' width={70}  height={70} /></Button>


            <CardActions />


        </Card>
    );
}
export default Cuadrado2
