import * as React from 'react';
import { styled, alpha } from '@mui/material/styles';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import IconButton from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';
import InputBase from '@mui/material/InputBase';
import MenuIcon from '@mui/icons-material/Menu';
import SearchIcon from '@mui/icons-material/Search';
import Image from "next/image";
import styles from "./page.module.css";
import { Button, Grid } from '@mui/material';
import { Margin } from '@mui/icons-material';
import Cuadrado from '@/Componentes/Cuadrado';
import Cuadrado2 from '@/Componentes/Cuadrado2';
import Tabla from '@/Componentes/Tabla';
import Footer from '@/Componentes/Footer';


export default function Home() {
  return (

    <Box >
      <Box sx={{ flexGrow: 1 }}>
        <AppBar sx={{ backgroundColor: "transparent", position: "relative" }}>
          <Toolbar >
            <Typography
              variant="h6"
              color="black"
              sx={{ flexGrow: 1, margin: "10px 0px", color: "FFFFFF", fontWeight: "700" }}
            >
              CryptoCap

            </Typography>

            <Grid sx={{ marginRight: "15%", fontWeight: "300" }} >
              <ul className='nav'>
                <li className='home'>Home</li>
                <li>Businesses</li>
                <li>Trade</li>
                <li>Market</li>
                <li>Learn</li>
              </ul>
            </Grid>
            <Box>
              <Grid sx={{ display: "flex" }}>
                < Grid sx={{ marginTop: "10px", marginRight: "100px" }}>
                  <Image src='/imagenes/mundo.svg ' alt="" width={100} height={100}></Image>
                </Grid>
                <Button sx={{ backgroundColor: "#0FAE96", position: "absolute", color: "#FFFFFF", borderRadius: "5pxs", textTransform: "none", marginLeft: "7% ", width: "7%" }}>Login</Button>
              </Grid>
            </Box>
          </Toolbar>
        </AppBar>

      </Box>
      <Grid sx={{ marginTop: "5%" }}>

        <Typography
          variant="h4"
          sx={{ fontWeight: "550", color: "#FFFFFF", marginRight: "20%", marginLeft: "25%" }}>
          Start and Build Your Crypto Portfolio Here
        </Typography>
        <Typography
          sx={{ color: "#B6B6B6", marginLeft: "33%" }}>
          Only at CryptoCap, you can build a good portfolio and learn
        </Typography>
        <Typography
          sx={{ color: "#B6B6B6", marginLeft: "40%" }}>
          best practices about cryptocurrency.
        </Typography>
        <Button sx={{ backgroundColor: "#0FAE96", color: "#FFFFFF", borderRadius: "6pxs", textTransform: "none", width: "10%", height: "7%", marginLeft: "45%", marginTop: "3%" }}>Get Started</Button>
      </Grid>
      <Grid sx={{ marginTop: "3% ", marginLeft: "3%" }}>
        <Typography sx={{ color: "#FFFFFF" }}>
          Market Trend
        </Typography>
      </Grid>
      <Box sx={{ display: "flex", justifyContent: "space-between", borderSpacing: "10px 5px" }}>
        <Cuadrado icono1="/imagenes/icon1.svg" letra="BTC" but="BITCOIN" num1="$56,623.54" num2="1.41%"></Cuadrado>
        <Cuadrado icono1="/imagenes/icon1.svg" letra="BTC" but="BITCOIN" num1="$56,623.54" num2="1.41%"></Cuadrado>
        <Cuadrado icono1="/imagenes/icon1.svg" letra="BTC" but="BITCOIN" num1="$56,623.54" num2="1.41%"></Cuadrado>
        <Cuadrado icono1="/imagenes/icon1.svg" letra="BTC" but="BITCOIN" num1="$56,623.54" num2="1.41%"></Cuadrado>

      </Box>
      <Grid>
        <Typography sx={{ fontSize: "200% ", color: "#FFFFFF", fontWeight: "600", marginLeft: "30%", marginTop: "5%" }}>
          CryptoCap Amazing Faetures
        </Typography>
        <Typography sx={{ marginLeft: "26%", marginTop: "2%", color: "#B6B6B6" }}>
          Explore sensational features to prepare your best investment in cryptocurrency
        </Typography>
      </Grid>
      <Grid sx={{ display: "flex", justifyContent: "space-between" }}>
        <Cuadrado2 icono2="/imagenes/icon2.svg" title="Manage Portfolio" parafo="Buy and sell popular digital currencies keep track of them in the one place." />
        <Cuadrado2 icono2="/nes/icon2.svg" title="Manage Portfolio" parafo="Buy and sell popular digital currencies, keep track of them in the one place." />
        <Cuadrado2  icono2="/imagenes/icon2.svg" title="Manage Portfolio" parafo="Buy and sell popular digital currencies keep track of them in the one place." />
        <Cuadrado2 icono2="/imagenes/icon2.svg" title="Manage Portfolio" parafo="Buy and sell popular digital currencies keep track of them in the one place." />
        <Cuadrado2 icono2="/imagenes/icon2.svg" title="Manage Portfolio" parafo="Buy and sell popular digital currencies keep track of them in the one place." />

      </Grid>
      <Box sx={{ marginTop: "5%", display: "flex" }}>
        <Grid sx={{ marginLeft: "2%", width: "80%" }} >
          <Typography variant="h4"
            sx={{ color: "#FFFFFF" }} >
            New In Cryptocurrency?

          </Typography>
          <Typography sx={{ color: "#B6B6B6", marginTop: "1.5%" }}>
            We'll tell you what cryptocurrencies are, how they work and why <br />you should own one right now. So let's do it.
          </Typography>

        </Grid >
        <Grid sx={{ marginRight: "10%", width: "20%" }}>
          <Button sx={{ backgroundColor: "#0FAE96", color: "#FFFFFF", borderRadius: "8px", textTransform: "none", width: "100%", height: "50%", marginLeft: "30%", marginTop: "20%" }}>Learn & Explore Now</Button>
        </Grid>
      </Box>
      <Box sx={{display:"flex"}}>
        <Grid sx={{ marginTop: "5%" , width:"60%", marginLeft:"2%" }}>

          <Typography
            variant="h4"
            sx={{ fontWeight: "550", color: "#FFFFFF", marginRight: "20%", marginTop: "5%" }}>
            How To Get Started
          </Typography>
          <Typography
            sx={{ color: "#B6B6B6", marginTop: "2%" }}>
            Simple and easy way to start your investment <br />
            in cryptocurrency</Typography>
          <Button sx={{ backgroundColor: "#0FAE96", color: "#FFFFFF", borderRadius: "6pxs", textTransform: "none", width: "30%", height: "10%", marginTop: "3%" }}>Get Started</Button>
        </Grid>
        <Grid sx={{ marginTop:"5%",width:"50%"  }}>   
         <Tabla icono3="/imagenes/icono2.svg" titulo="Create Your Account" para="Your account and personal identity are guaranteed safe."></Tabla> 
         <Tabla icono3="/imagenes/icono2.svg" titulo="Create Your Account" para="Your account and personal identity are guaranteed safe."></Tabla> 
         <Tabla icono3="/imagenes/icono2.svg" titulo="Create Your Account" para="Your account and personal identity are guaranteed safe."></Tabla> 
        </Grid>
      </Box>
      
      <Grid>
        <Typography sx={{ fontSize: "200% ", color: "#FFFFFF", fontWeight: "600", marginLeft: "30%", marginTop: "5%" }}>
        Learn About Cryptocurrency
        </Typography>
        <Typography sx={{ marginLeft: "33%", marginTop: "2%", color: "#B6B6B6" }}>
        Learn all about cryptocurrency to start investing        </Typography>
      </Grid>
      <Box sx={{display:"flex" , marginTop:"5%"}}>
        <Footer title="CryptoCap " image="/imagenes/footer.svg"></Footer>
        <Footer  title="About Us" text="About " text1="Careers" text3="Blog" text4="Legal & privacy"/>
        <Footer  title="About Us" text="About " text1="Careers" text3="Blog" text4="Legal & privacy"/>
        <Footer  title="About Us" text="About " text1="Careers" text3="Blog" text4="Legal & privacy"/>
        <Footer  title="About Us" text="About " text1="Careers" text3="Blog" text4="Legal & privacy"/>

      </Box>


    </Box>
  );
}